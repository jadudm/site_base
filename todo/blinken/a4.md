---
---


## Current-Limiting Resistors

The Bald Engineer also talks about current-limiting resistors. It's a nice video, and I think you should watch it. 

<div class="row">
<div class="text-center">
    <iframe width="640" height="360" src="//www.youtube.com/embed/81zNcctopBI" frameborder="0" allowfullscreen></iframe>
</div>
</div>

If there are things you don't understand, you should write down some questions in your notebook, and come in prepared to ask them in class.

## Learning Objectives

* **Theory 1.4**: Know safe limits for voltage and current (for people).

* **Components 1.2**: Demonstrate knowledge of passive components.


## Submission

None. However, you are expected to have an understanding of how resistors work in a circuit (as exemplified in this video) as part of future writing assignments and examinations. Come to class prepared to ask questions.
