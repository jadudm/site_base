# Clone all the interesting bits.
git clone https://jadudm@bitbucket.org/jadudm/site_utils.git             _utils
git clone https://jadudm@bitbucket.org/jadudm/site_plugins.git           _plugins
git clone https://jadudm@bitbucket.org/jadudm/site_layouts.git           _layouts
git clone https://jadudm@bitbucket.org/jadudm/site_js.git                js
git clone https://jadudm@bitbucket.org/jadudm/site_includes.git          _includes
git clone https://jadudm@bitbucket.org/jadudm/site_images_common.git     images_common
git clone https://jadudm@bitbucket.org/jadudm/site_css.git               css

# Ignore them.
echo "_utils" >> .gitignore
echo "_plugins" >> .gitignore
echo "_layouts" >> .gitignore
echo "js" >> .gitignore
echo "_includes" >> .gitignore
echo "images_common" >> .gitignore
echo "css" >> .gitignore

git commit -am "Adding in submodules for course website."
git push
