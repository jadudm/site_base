dirs=( _utils _plugins _layouts js _includes images_common css )

for d in "${dirs[@]}"
do
  echo Updating in directory ${d}
  pushd ${d}
    git pull
  popd
done
